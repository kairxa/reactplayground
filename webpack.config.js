var path    = require('path');
var webpack = require('webpack');
var nodeDir = path.resolve(__dirname, 'node_modules');

var config = {
    entry: {
        admin: path.resolve(__dirname, 'build/assets/jsx/admin.js'),
        vendors: [
            'react',
            'react-dom',
            'react-router',
            'react-redux',
            'redux',
            'redux-react-router',
            'redux-thunk',
            'isomorphic-fetch'
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist/assets/javascripts'),
        filename: '[name].js',
        publicPath: './assets/javascripts/'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: [nodeDir],
                loader: 'babel-loader',
                query: { stage: 0 }
            }
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
    ]
};

module.exports = config;