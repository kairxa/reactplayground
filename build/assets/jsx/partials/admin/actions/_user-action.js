/**
 * Created by kairxa on 9/13/15.
 */

import {
    SHOW_USERS_ALL,
    SHOW_USERS_EVEN,
    SHOW_USERS_ODD,

    FETCH_USERS_REQUESTING,
    FETCH_USERS_FAILED,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FINISHED
} from '../constants/_list-actions.js';
import { setCache, getCache } from '../_caching-helper.js';

import fetch from 'isomorphic-fetch';

function requestingUsers(usersType) {
    return {
        type: FETCH_USERS_REQUESTING,
        usersType
    };
}

function failedRequestingUsers(errorMessage) {
    return {
        type: FETCH_USERS_FAILED,
        errorMessage
    };
}

function successRequestingUsers(responseMessage) {
    return {
        type: FETCH_USERS_SUCCESS,
        responseMessage
    }
}

function finishedRequestingUsers() {
    return {
        type: FETCH_USERS_FINISHED
    };
}

export function fetchUsers(usersType) {
    return function(dispatch) {

        // Dispatching event that we are currently requesting users
        dispatch(requestingUsers(usersType));

        // Don't forget that sessionStorage stores string instead of object
        var cache = getCache('users-' + usersType);

        if(cache) {
            // Therefore parsing the cache string is a required step in here
            dispatch(successRequestingUsers(JSON.parse(cache)));
            dispatch(finishedRequestingUsers());
        } else {
            // Do try catch here
            fetch('http://jsonplaceholder.typicode.com/users')
                .then(function(response) {
                    if (response.status >= 200 && response.status < 300) {
                        return response.json();
                    }
                })
                .then(function(json) {
                    setCache('users-' + usersType, JSON.stringify(json));
                    console.log(json);
                    dispatch(successRequestingUsers(json));
                })
                .then(function() {
                    dispatch(finishedRequestingUsers());
                });
        }
    }
}

export function filterUsers(filterType, users) {
    return {
        type: filterType,
        users
    }
}