/**
 * Created by kairxa on 9/15/15.
 */

import { combineReducers } from 'redux';
import { routerStateReducer } from 'redux-react-router';

import {
    SHOW_USERS_ALL,
    SHOW_USERS_EVEN,
    SHOW_USERS_ODD,

    FETCH_USERS_REQUESTING,
    FETCH_USERS_FAILED,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FINISHED
} from '../constants/_list-actions.js';

function users(state = { // Giving initial state; should we add usersType too?
    isFetching: false,
    didFinish: true,
    users: [],
    error: null,
    filterType: SHOW_USERS_ALL
}, action = null) {
    switch(action.type) {
        case FETCH_USERS_REQUESTING:
            return Object.assign({}, state, { // Notifying that we are currently fetching users
                isFetching: true,
                didFinish: false
            });
        case FETCH_USERS_FINISHED:
            return Object.assign({}, state, { // Notifying that fetching users is finished
                isFetching: false,
                didFinish: true
            });
        case FETCH_USERS_FAILED:
            return Object.assign({}, state, { // Notifying that fetching users was failed - returning errorMessage
                error: 'Error oi'
            });
        case FETCH_USERS_SUCCESS:
            return Object.assign({}, state, { // Notifying that fetching users was success - returning users
                users: action.responseMessage,
                originalUsers: action.responseMessage
            });
        case SHOW_USERS_ALL:
            return Object.assign({}, state, {
                filterType: action.type,
                users: state.originalUsers
            });
        case SHOW_USERS_EVEN:
            return Object.assign({}, state, {
                filterType: action.type,
                users: state.originalUsers.filter(user => user.id % 2 === 0)
            });
        case SHOW_USERS_ODD:
            return Object.assign({}, state, {
                filterType: action.type,
                users: state.originalUsers.filter(user => user.id %2 !== 0)
            });
        default:
            return state;
    }
}

const userReducer = combineReducers({
    users,
    router: routerStateReducer
});

export default userReducer;