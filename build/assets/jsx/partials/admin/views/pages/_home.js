/**
 * Created by kairxa on 9/2/15.
 */

import React from 'react';

class Home extends React.Component {
    render() {
        return (
            <div className="home">
                <h4>Hello. Welcome to KreasiKamu Admin Page. Pick your menu on the left.</h4>
            </div>
        );
    }
}

module.exports = Home;