/**
 * Created by kairxa on 9/7/15.
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import {
    SHOW_USERS_ALL,

    FETCH_USERS_REQUESTING,
    FETCH_USERS_FAILED,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FINISHED
} from '../../constants/_list-actions.js';
import { fetchUsers, filterUsers } from '../../actions/_user-action.js';

import UserNav from '../layout/users/_nav.js';
import UserTable from '../components/users/_table.js';

function preparation(state) {
    return {
        visibilityFilter: state.visibilityFilter,
        users: state.users.users
    };
}

@connect( preparation => preparation ) // mapping state.users from _user-reducer
class User extends React.Component {
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchUsers(SHOW_USERS_ALL));
    }

    render() {
        const { dispatch } = this.props;
        const { isFetching, didFinish, error, users } = this.props.users;

        const filters = [
            {
                id: 1,
                filter: 'SHOW_USERS_ALL'
            },
            {
                id: 2,
                filter: 'SHOW_USERS_EVEN'
            },
            {
                id: 3,
                filter: 'SHOW_USERS_ODD'
            }
        ];

        let displayedThing;

        if (isFetching) {
            displayedThing = (
                <div>Fetching users..</div>
            );
        }

        if (error) {
            displayedThing = (
                <div>Error nih.</div>
            );
        }

        if (users.length !== 0) {
            displayedThing = (
                <UserTable
                    users={ users }>
                </UserTable>
            );
        }

        return (
            <div>
                <UserNav
                    filters={ filters }
                    onFilterChange={ type => { dispatch(filterUsers(type, users)) } }
                    >
                </UserNav>
                {displayedThing}
            </div>
        );
    }
}

module.exports = User;