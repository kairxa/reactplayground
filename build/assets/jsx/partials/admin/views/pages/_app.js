/**
 * Created by putra.tanzil on 9/16/15.
 */

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Header from '../layout/_header.js';
import Nav from '../layout/_nav.js';
import { fetchUsers, filterUsers } from '../../actions/_user-action.js';

@connect(
    dispatch => bindActionCreators({fetchUsers, filterUsers}, dispatch)
)
class App extends React.Component {
    render() {
        return (
            <div>
                <Header></Header>
                <main className="container-fluid">
                    <div className="row">
                        <Nav></Nav>
                        <section className="main-app col-xs-9">
                            { this.props.children }
                        </section>
                    </div>
                </main>
            </div>
        );
    }
}

module.exports = App;