/**
 * Created by kairxa on 9/23/15.
 */

import React from 'react';

class UserTable extends React.Component {
    render() {
        return (
            <div>
                { this.props.users.map(function(user) {
                    return (
                        <div key={ user.id }>{ user.email }</div>
                    );
                })}
            </div>
        );
    }
}

module.exports = UserTable;