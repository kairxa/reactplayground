/**
 * Created by kairxa on 9/7/15.
 */

import React from 'react';
import { Link } from 'react-router';

class UserNav extends React.Component {
    render() {
        return (
            <nav className="sub-nav">
                <Link to="specific-user" params={{userId: 'test'}} className="sub-nav-link">Test</Link>
                { this.props.filters.map(type => {
                    return (
                        <button key={ type.id } onClick={ () => { this.props.onFilterChange(type.filter) } }>{ type.filter }</button>
                    )
                })}
            </nav>
        );
    }
}

module.exports = UserNav;