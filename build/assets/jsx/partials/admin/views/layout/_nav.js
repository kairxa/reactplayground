/**
 * Created by kairxa on 9/2/15.
 */

import React from 'react';
import { Link } from 'react-router';

class Nav extends React.Component {
    render() {
        return (
            <nav className="admin-nav col-xs-3">
                <Link to="/home" className="admin-nav-link">
                    <div className="u-text-group">
                        <div className="u-text-group-addon">
                            <img src="//placehold.it/60x60" alt="test" className="u-text-group-addon-img"/>
                        </div>
                        <div className="u-text">
                            Home
                        </div>
                    </div>
                </Link>
                <Link to="/user" className="admin-nav-link">
                    <div className="u-text-group">
                        <div className="u-text-group-addon">
                            <img src="//placehold.it/60x60" alt="test" className="u-text-group-addon-img"/>
                        </div>
                        <div className="u-text">
                            User
                        </div>
                    </div>
                </Link>
            </nav>
        );
    }
}

module.exports = Nav;