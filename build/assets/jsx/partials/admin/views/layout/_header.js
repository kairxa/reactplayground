/**
 * Created by kairxa on 8/31/15.
 */

import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <header className="header">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-12">
                            <h1 className="header-title">KreasiKamu Admin Page</h1>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

module.exports = Header;