/**
 * Created by putra.tanzil on 9/16/15.
 */

import React from 'react';
import { Router, Route, DefaultRoute } from 'react-router';

import App from './views/pages/_app.js';
import Home from './views/pages/_home.js';
import User from './views/pages/_user.js';

const Routes = (
    <Route path="/" component={App}>
        <Route path="/home" component={Home}></Route>
        <Route path="/user" component={User}>
            <Route path=":userId" component={User}></Route>
        </Route>
    </Route>
);

module.exports = Routes;