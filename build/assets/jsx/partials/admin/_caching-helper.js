/**
 * Created by kairxa on 9/25/15.
 */

function storageAvailable(type) {
    // First we check whether we already initialized `type` storage checking or not
    if(window.isStorageAvailable && type in window.isStorageAvailable) {
        return true;
    } else {
        // If we haven't, the window.isStorageAvailable[type] should be empty. Therefore, try catch.
        try {
            var storage = window[type],
                x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);

            /*
             If the above failed, then catch would automatically get something so this shouldn't be triggered.
             Anyway, saving state to window that we have already initialized `type` storage checking.
              */
            if(window.isStorageAvailable) {
                window.isStorageAvailable[type] = true;
            } else {
                window.isStorageAvailable = [];
                window.isStorageAvailable[type] = true;
            }

            return true;
        }
        catch(e) {
            return false;
        }
    }
}

export function setCache(cacheKey, cacheItem) {

    if(storageAvailable('sessionStorage')) {
        sessionStorage.setItem(cacheKey, cacheItem);
        return {
            isSuccess: true,
            message: 'Cache ' + cacheKey + ' was successfully saved.'
        };
    } else {
        return {
            isSuccess: false,
            message: 'Cache ' + cacheKey + ' failed to be saved. Maybe your browser is too old?'
        }
    }

}

export function getCache(cacheKey) {

    if(storageAvailable('sessionStorage')) {
        return sessionStorage.getItem(cacheKey);
    } else {
        return null;
    }

}

export function clearCache(cacheKey) {

    if(storageAvailable('sessionStorage')) {
        sessionStorage.removeItem(cacheKey);
        return {
            isSuccess: true,
            message: 'Cache ' + cacheKey + ' was successfully removed'
        }
    } else {
        return {
            isSuccess: false,
            message: 'Cache ' + cacheKey + ' failed to be deleted. Maybe it doesn\'t exist or your browser is too old.'
        }
    }

}