/**
 * Created by kairxa on 9/15/15.
 */

import {
    compose,
    createStore,
    applyMiddleware
} from 'redux';
import { reduxReactRouter } from 'redux-react-router';
import createHistory from 'react-router/node_modules/history/lib/createHashHistory.js';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import { devTools } from 'redux-devtools';
import userReducer from '../reducers/_user-reducer.js';
import routes from '../_routes.js';

const loggerMiddleware = createLogger();

const createStoreWithMiddleware = compose(
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    ),
    reduxReactRouter({
        routes,
        createHistory
    }),
    devTools()
)(createStore)(userReducer);

module.exports = createStoreWithMiddleware;