/**
 * Created by kairxa on 9/13/15.
 */

import keyMirror from 'key-mirror';

export default keyMirror({

    SHOW_USERS_ALL: null,
    SHOW_USERS_EVEN: null,
    SHOW_USERS_ODD: null,

    FETCH_USERS_REQUESTING: null,
    FETCH_USERS_FAILED: null,
    FETCH_USERS_SUCCESS: null,
    FETCH_USERS_FINISHED: null

});