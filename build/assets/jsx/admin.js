/**
 * Created by kairxa on 8/31/15.
 */

import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Route } from 'react-router';
import { createStore, compose, combineReducers, bindActionCreators } from 'redux';
import { Provider } from 'react-redux';
import { ReduxRouter } from 'redux-react-router';
import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';

import userReducer from './partials/admin/reducers/_user-reducer.js';
import configureStore from './partials/admin/stores/_user-store.js';

const store = configureStore;

class Root extends React.Component {
    render() {
        return (
            <div>
                <Provider store={store}>
                    <ReduxRouter></ReduxRouter>
                </Provider>
                <DebugPanel top right bottom>
                    <DevTools store={store} monitor={LogMonitor}></DevTools>
                </DebugPanel>
            </div>
        );
    }
}

ReactDOM.render(
    <Root></Root>,
    document.querySelector('.App')
);